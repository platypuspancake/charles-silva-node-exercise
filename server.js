const express = require('express')
const fetch = require('node-fetch')

const app = express()
const port = 8080

function getAllPages(url) { // Recursively paginates all pages of a given endpoint and returns an array of all elements
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(res => res.json())
      .then(json => {
        let elementsArray = json.results
        if (json.next) { // If a next page exists get the results of that page and concatenate them to our array
          getAllPages(json.next)
            .then(elementsArrayBelow => {
              let arrayToReturn = elementsArray.concat(elementsArrayBelow)
              resolve(arrayToReturn)
            })
        }
        else { // When we reach the last page return that page
          resolve(elementsArray)
        }
      })
  })
}

function getParam(url, param) { // Requests a URL and returns the parameter specified
  return new Promise((resolve, reject) => {
    return fetch(url)
      .then(res => res.json())
      .then(json => {
        resolve(json[param])
      })
  })
}

function sortByParam(param) { // Sorts an array of objects by a given param
  if (param == 'name') { // Alphabetic params
    return function (a, b) {
      return (a[param] < b[param]) ? -1 : 1
    }
  }
  else { // Numeric params
    return function (a, b) {
      aParsed = Number(a[param].split(",").join("")) // Remove commas and convert to Number
      bParsed = Number(b[param].split(",").join(""))

      if (isNaN(aParsed)) { // Send non-numerical answers to the bottom of the list
        return 1
      }
      else if (isNaN(bParsed)) {
        return -1
      }

      return (aParsed < bParsed) ? -1 : 1
    }
  }
}

function updatePlanetResidents(planetsArray) { // Update planet residents from URLs to names
  return new Promise ((resolve, reject) => {
    let planetPromiseArray = []

    planetsArray.forEach(planet => { // Go through each planet and create a promise for when their residents array is updated
      let planetPromise = new Promise((resolve, reject) => {
        let namePromiseArray = []
        
        planet.residents.forEach(resident => { // Go through each resident and create a promise for when their name is returned
          let namePromise = getParam(resident, 'name')
          namePromiseArray.push(namePromise)
        })

        Promise.all(namePromiseArray) // Wait for all names to be returned
          .then(names => {
            planet.residents = names // Update the residents array with the names
            resolve()
          })
      })

      planetPromiseArray.push(planetPromise)
    })

    Promise.all(planetPromiseArray) // Wait for all planets to be updated
      .then(() => {
        resolve()
      })
  })
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/people', (req, res) => {
  if (req.query.sortBy) { // Check if there is a sortBy query, and if it is a valid search parameter
    let validSearchParams = ['name', 'height', 'mass']

    if (!validSearchParams.includes(req.query.sortBy)) {
      throw('Invalid search parameter')
    }
  }

  getAllPages('https://swapi.dev/api/people')
    .then(peopleArray => {
      if (req.query.sortBy) {
        peopleArray.sort(sortByParam(req.query.sortBy)) // Sort the array by the specified parameter
      }

      res.json(peopleArray)
    })
    .catch((err) => {
      res.status(400).send({
        message: err
      })
      console.error(err)
    })
})

app.get('/planets', (req, res) => {
  getAllPages('https://swapi.dev/api/planets')
    .then(planetsArray => {
      updatePlanetResidents(planetsArray)
        .then(() => {
          res.json(planetsArray)
        })
    })
    .catch((err) => {
      res.status(400).send({
        message: err
      })
      console.error(err)
    })
})

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})